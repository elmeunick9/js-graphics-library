import Vector from "./vector.js"
import Matrix from "./matrix.js"
import { cube } from "./model.js"

export function vertex(x,y,z) {
	return [x,y,z];
}

export function face(a,b,c) {
	return [a,b,c];
}

function defaultVertexShader(obj, p, i) {
	var mat = obj.uniforms["modelViewProjectionMatrix"];
	var normal = obj.normals[Math.floor(i/3)];
	
	obj.vertex_out["vcolor"][i] = new Vector(Math.abs(normal.x), Math.abs(normal.y), Math.abs(normal.z));
	return mat.transformPoint(p);
}

function defaultFragmentShader(obj, x, y, z) {
	var vcolor = obj.vertex_in["vcolor"][y][x];
	var normalMatrix = obj.uniforms["normalMatrix"];
	
	var c = normalMatrix.transformVector(vcolor);
	return new Vector(c.z, c.z, c.z);
}

export class Object {
	constructor() {
		this.model = cube();
		this.normals = this.calculateNormals();
		
		this.modelMatrix = new Matrix();
		this.vertexShader = defaultVertexShader;
		this.fragmentShader = defaultFragmentShader;
		this.uniforms = {};
		this.vertex_out = {};
		this.vertex_in = {};
		this.orientation = [{x:0,y:0,z:0},{x:0,y:0,z:0}];
		this.position = [{x:0,y:0,z:0},{x:0,y:0,z:0}];
		this.scale = {x:1,y:1,z:1};
	}
	
	//Can be optimized
	updateTransform() {
		var oL = this.orientation[0];
		var oG = this.orientation[1];
		var p = this.position;
		var s = this.scale;
		
		var X = Matrix.rotate(oL.x, 1, 0, 0);
		var Y = Matrix.rotate(oL.y, 0, 1, 0);
		var Z = Matrix.rotate(oL.z, 0, 0, 1);
		
		var posL = Matrix.translate(p[0].x, p[0].y, p[0].z);
		var rotL = Matrix.multiply(Matrix.multiply(Z, Y), X);
		var sca = Matrix.scale(s.x, s.y, s.z);
		
		this.modelMatrix = Matrix.multiply(rotL, posL); //Scale here
		
		var posG = Matrix.translate(p[1].x, p[1].y, p[1].z);
		X = Matrix.rotate(oG.x, 1, 0, 0, X);
		Y = Matrix.rotate(oG.y, 0, 1, 0, Y);
		Z = Matrix.rotate(oG.z, 0, 0, 1, Z);
		var rotG = Matrix.multiply(Matrix.multiply(Z, Y), X);
		
		var worldMatrix = Matrix.multiply(rotG, posG);
		this.modelMatrix = Matrix.multiply(worldMatrix, this.modelMatrix);	
	}
	
	displace(x,y,z) {
		var oL = this.orientation[0];
		var p = new Vector(x,y,z);
		
		var X = Matrix.rotate(oL.x, 1, 0, 0);
		var Y = Matrix.rotate(oL.y, 0, 1, 0);
		var Z = Matrix.rotate(oL.z, 0, 0, 1);
		var rotL = Matrix.multiply(Matrix.multiply(Z, Y), X);
		
		p = Matrix.inverse(rotL).transformVector(p);
		this.position[1].x += p.x;
		this.position[1].y += p.y;
		this.position[1].z += p.z;
	}
	
	calculateNormals() {
		var normals = new Array(this.model.length);
		for (var j = 0; j < this.model.length; j++) {
			var p0 = this.model[j][0];
			var p1 = this.model[j][1];
			var p2 = this.model[j][2];
			
			var a = new Vector(p1[0] - p0[0], p1[1] - p0[1], p1[2] - p0[2]);
			var b = new Vector(p2[0] - p0[0], p2[1] - p0[1], p2[2] - p0[2]);			
			
			var n = a.cross(b);
			normals[j] = n;
		}
		return normals;
	}
	
	makeBuffer(name) {
		 this.vertex_out[name] = new Array(this.model.length*3);
		 this.vertex_in[name] = undefined;
	}
}

export class Camera {
	constructor(position, fov, aspect, near, far) {
		this.projectionMatrix = Matrix.perspective(fov, aspect, near, far);
		this.viewMatrix = Matrix.translate(position.x, position.y, position.z);
		this.orientation = [{x:0,y:0,z:0},{x:0,y:0,z:0}];
		this.position = [position,{x:0,y:0,z:0}];
	}
	
	//Can be optimized
	updateTransform() {
		var oL = this.orientation[0];
		var oG = this.orientation[1];
		var p = this.position;
		
		var X = Matrix.rotate(oL.x, 1, 0, 0);
		var Y = Matrix.rotate(oL.y, 0, 1, 0);
		var Z = Matrix.rotate(oL.z, 0, 0, 1);
		
		var posL = Matrix.translate(p[0].x, p[0].y, p[0].z);
		var rotL = Matrix.multiply(Matrix.multiply(Z, Y), X);
		
		this.viewMatrix = Matrix.multiply(rotL, posL);
		
		var posG = Matrix.translate(p[1].x, p[1].y, p[1].z);
		X = Matrix.rotate(oG.x, 1, 0, 0, X);
		Y = Matrix.rotate(oG.y, 0, 1, 0, Y);
		Z = Matrix.rotate(oG.z, 0, 0, 1, Z);
		var rotG = Matrix.multiply(Matrix.multiply(Z, Y), X);
		
		var worldMatrix = Matrix.multiply(rotG, posG);
		this.viewMatrix = Matrix.multiply(worldMatrix, this.viewMatrix);	
	}
	
	displace(x,y,z) {
		var oL = this.orientation[0];
		var p = new Vector(x,y,z);
		
		var X = Matrix.rotate(oL.x, 1, 0, 0);
		var Y = Matrix.rotate(oL.y, 0, 1, 0);
		var Z = Matrix.rotate(oL.z, 0, 0, 1);
		var rotL = Matrix.multiply(Matrix.multiply(Z, Y), X);
		
		p = rotL.transformVector(p);
		this.position[1].x += p.x;
		this.position[1].y += p.y;
		this.position[1].z += p.z;
	}

}

export class Scene {
	constructor() {
		this.camera = new Camera({x:0,y:0,z:-3}, 60, 2, 0.01, 20);
	}
}

export function pointInTriangle(p, p0, p1, p2) {
	var dX = p[0]-p2[0];
	var dY = p[1]-p2[1];
	var dX21 = p2[0]-p1[0];
	var dY12 = p1[1]-p2[1];
	var D = dY12*(p0[0]-p2[0]) + dX21*(p0[1]-p2[1]);
	var s = dY12*dX + dX21*dY;
	var t = (p2[1]-p0[1])*dX + (p0[0]-p2[0])*dY;
	if (D<0) return s<=0 && t<=0 && s+t>=D;
	return s>=0 && t>=0 && s+t<=D;
}

//2D - https://codeplea.com/triangular-interpolation
// May be improved by storing partial results
export function triangleInterpol(t, p1, p2, p3) {
	var dY23 = p2[1] - p3[1];
	var dX32 = p3[0] - p2[0];
	var dX13 = p1[0] - p3[0];
	var dY13 = p1[1] - p3[1];
	var dY31 = p3[1] - p1[1];
	var denom = dY23*dX13 + dX32*dY13;
	
	var out = new Array(t.length);
	for (var i = 0; i < t.length; i++) {
		var dXp3 = t[i][0] - p3[0];
		var dYp3 = t[i][1] - p3[1];
	
		var W1 = (dY23*dXp3 + dX32*dYp3) / denom;
		var W2 = (dY31*dXp3 + dX13*dYp3) / denom;
		var W3 = 1 - W1 - W2;
		out[i] = [W1,W2,W3]
	}
	
	return out;
}

export {Matrix, Vector};
import g from './jsg.js'
	
g.init("screen", 8);
g.setBackgroundColor(g.rgb(0,0.1,0.2));

setInterval(() => {
    g.drawObject(obj);
    g.render();
}, 1/30);

document.onkeydown = (e) => {
    var cam = g.getScene().camera;
    if ( e.code === 'KeyW' ) {
        cam.orientation[0].x -= 1;
    }

    if ( e.code === 'KeyS') {
        cam.orientation[0].x += 1;
    }

    if ( e.code === 'KeyA' ) {
        cam.orientation[0].y += 1;
    }

    if ( e.code === 'KeyD' ) {
        cam.orientation[0].y -= 1;
    }
    
    if ( e.code === 'Space' ) {
        cam.displace(0,0,0.1);
    }
    
    cam.updateTransform();
}

document.onmousemove = () => {
    /*
    var pos = {x:e.pageX, y:e.pageY};
    
    var dx = pos.x - lastMousePos.x;
    var dy = pos.y - lastMousePos.y;
    
    obj.orientation[0].x += dx;
    obj.orientation[0].z += dy;
    obj.updateTransform();
    g.getScene().camera.rotate(dx/5, 0, dy/5);
    
    lastMousePos = pos;
    */
}

g.getScene().camera.position[1].x = -1;
g.getScene().camera.position[1].y = -1;

g.getScene().camera.orientation[0].x = 20;
g.getScene().camera.orientation[0].y = -20;
g.getScene().camera.updateTransform();

var obj = new g.Object();
obj.makeBuffer("vcolor");

document.g = g;
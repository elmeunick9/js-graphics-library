import {Vector, Matrix, Object, triangleInterpol, Scene} from './base.js'

var canvas;

var truebuffer;
var frontbuffer;
var backbuffer;
var cleanbuffer;

var backgroundColor = {r:0,g:0,b:0,a:0};
var scene = new Scene();

export function getScene() {
	return scene;
}

export function getBuffer() {
	return frontbuffer;
}

export function setBackgroundColor(color) {
	backgroundColor = color;
}

function initBuffer(sx, sy, color={r:0,g:0,b:0,a:0}) {
	var buffer = {
		"color" : new Array(sy),
		"zbuffer" : new Array(sy),
	};
	for (var i = 0; i < sy; i++) {
		buffer.color[i] = new Array(sx);
		buffer.zbuffer[i] = new Array(sx);
		for (var j = 0; j < sx; j++) {
			buffer.color[i][j] = color;
			buffer.zbuffer[i][j] = 1;
		}
	}
	return buffer;
}
	
export function init(cname, scale) {
	canvas = document.getElementById(cname);
	
	frontbuffer = initBuffer(canvas.width/scale, canvas.height/scale);
	backbuffer = initBuffer(canvas.width/scale, canvas.height/scale);
}

export function render() {
	var canvasWidth = canvas.width;
	var canvasHeight = canvas.height;
	var ctx = canvas.getContext('2d');
	ctx.clearRect(0, 0, canvasWidth, canvasHeight);
	
	if (truebuffer === undefined) {
		truebuffer = ctx.getImageData(0, 0, canvasWidth, canvasHeight);
	}
	
	var cbuff = frontbuffer.color;
	var sy = cbuff.length;
	var sx = cbuff[0].length;
	var scale = truebuffer.width/sx;
	var pixels = truebuffer.data;

	for (var ty = 0; ty < sy; ty++) {
		for (var tx = 0; tx < sx; tx++){
			for (var j = 0; j < scale; j++) {
				for (var i = 0; i < scale; i++) {
					var x = tx * scale + j;
					var y = ty * scale + i;
					var off = (y * truebuffer.width + x) * 4;
					pixels[off] = cbuff[ty][tx].r;
					pixels[off + 1] = cbuff[ty][tx].g;
					pixels[off + 2] = cbuff[ty][tx].b;
					pixels[off + 3] = 255;
				}
			}
			
		}
	}

	ctx.putImageData(truebuffer, 0, 0);
	
	//Swicth buffers
	frontbuffer = backbuffer;
	backbuffer = initBuffer(canvas.width/scale, canvas.height/scale, backgroundColor);
}

//Utils
export function rgba(rc,gc,bc,ac) {
	return {r:rc*255,g:gc*255,b:bc*255,a:ac*255};
}
export function rgb(rc,gc,bc) {
	return {r:rc*255,g:gc*255,b:bc*255,a:0};
}

function lerp(a, b, p) {
	var r = (b.r-a.r)*p + a.r;
	var g = (b.g-a.g)*p + a.g;
	var b = (b.b-a.b)*p + a.b;
	return {r:r,g:g,b:b,a:255};
}

export function drawPixel(x, y, color) {
	if (y < 0 || y >= frontbuffer.color.length) return;
	if (x < 0 || x >= frontbuffer.color[0].length) return;
	if (color.a == 0) return;
	if (color.a == 255) {
		frontbuffer.color[y][x] = color;
		return;
	}
	frontbuffer.color[y][x] = lerp(frontbuffer.color[y][x], color, color.a/255);
}

export class Sprite {
	constructor(width, height, data) {
		this.height = height;
		this.width = width;
		this.data = data;
		
		if (this.data === undefined) {
			this.data = initBuffer(width, height).color;
		}
	}
	
	drawPixel(x, y, color) {
		this.data[y][x] = color;
	}
}

export function drawSprite(obj, x, y) {
	for (var j = 0; j < obj.height; j++) {
		for (var i = 0; i < obj.width; i++) {
			drawPixel(x+i, y+j, obj.data[j][i]);
		}
	}
}

//face is in screen coordinates.
function drawTriangle(a, b, c, obj, i) {
	//Quality can be improved by a lot using alpha blending on edges.
	
	var cbuff = frontbuffer.color;
	var sy = cbuff.length;
	var sx = cbuff[0].length;
	
	//Bounidng box
	var min = [Math.floor(Math.min(a[0], b[0], c[0])), Math.ceil(Math.min(a[1], b[1], c[1]))];
	var max = [Math.floor(Math.max(a[0], b[0], c[0])), Math.ceil(Math.max(a[1], b[1], c[1]))];
	
	min = [Math.max(min[0], 0),  Math.max(min[1], 0)];
	max = [Math.min(max[0], sx), Math.min(max[1],sy)];
	
	function processFragment(W, x, y, has_color=undefined) {
		var d = -0;
		if (W[0] >= d && W[1] >= d && W[2] >= d) {
			if (has_color !== undefined) return has_color;
			
			for (var name in obj.vertex_in) {
				var v0 = obj.vertex_out[name][i*3+0].multiply(W[0]);
				var v1 = obj.vertex_out[name][i*3+1].multiply(W[1]);
				var v2 = obj.vertex_out[name][i*3+2].multiply(W[2]);
				
				obj.vertex_in[name][y][x] = v0.add(v1).add(v2);
			}
			
			var z = a[2]*W[0] + b[2]*W[1] + c[2]*W[2];
			var color = obj.fragmentShader(obj,x,y,z);
			
			//Depth test
			if (z > frontbuffer.zbuffer[y][x]) return null;
			frontbuffer.zbuffer[y][x] = z;
			
			return rgba(color.x, color.y, color.z, 1);
		}
	}
	
	for (var y = min[1]; y < max[1]; y++) {
		for (var x = min[0]; x < max[0]; x++) {
			// With x16 antialiasing
			var color = undefined;
			var alpha = 0;
			
			var k = 16;
			var p_list = new Array(k);
			for (var kj = 0; kj < k; kj++) {
				var dX = (kj % 4) / 4;
				var dY = Math.floor(kj/4)/4;
				p_list[kj] = [x+dX,y+dY];
			}
			
			var W = triangleInterpol(p_list, a, b, c);
			
			for (var kj = 0; kj < k; kj++) {
				var col = processFragment(W[kj], x, y, color);
				if (col !== undefined) {
					if (col != null) color = col;
					alpha += 255;
				}					
			}
			
			if (color !== undefined) {
				color.a = alpha / k;
				drawPixel(x,y,color);
			}
			
			
			/* // No antialiasing
			var W = triangleInterpol([[x+0.5,y+0.5]], a, b, c)[0];	
			var color = processFragment(W, x, y);
			if (color !== undefined) drawPixel(x,y,color);
			*/ 
		}
	}
}

export function drawObject(obj) {
	var cbuff = frontbuffer.color;
	var sy = cbuff.length;
	var sx = cbuff[0].length;
	
	var mvm = Matrix.multiply(scene.camera.viewMatrix, obj.modelMatrix);
	var vpm = Matrix.multiply(scene.camera.projectionMatrix, scene.camera.viewMatrix);
	var mvpm = Matrix.multiply(vpm, obj.modelMatrix);
	var normalMatrix = mvm.inverse().transpose();
	
	obj.uniforms["modelViewMatrix"] = mvm;
	obj.uniforms["modelViewProjectionMatrix"] = mvpm;
	obj.uniforms["normalMatrix"] = normalMatrix;
	
	//Initialize vertex_out
	for (var name in obj.vertex_out) {
		obj.vertex_in[name] = new Array(sy);
		for (var y = 0; y < sy; y++) {
			obj.vertex_in[name][y] = new Array(sx);
		}
	}
	
	for (var i = 0; i < obj.model.length; i++) {
		var face = obj.model[i];
		var normal = obj.normals[i];
		
		//Vertex tranforms (vertex shader)
		var a = new Vector(face[0][0], face[0][1], face[0][2]);
		var b = new Vector(face[1][0], face[1][1], face[1][2]);
		var c = new Vector(face[2][0], face[2][1], face[2][2]);
		
		a = obj.vertexShader(obj, a, i*3+0);
		b = obj.vertexShader(obj, b, i*3+1);
		c = obj.vertexShader(obj, c, i*3+2);
		
		//Perspective Division is done within the vertex shader.
		
		//Do clipping
		
		//Backface culling
		normal = normalMatrix.transformVector(normal);
		var t = new Vector(face[0][0], face[0][1], face[0][2]);
		t = mvm.transformVector(t);
		t = t.negative().dot(normal);
		if (t >= 0) continue;
		
		//Convert form NDC to Screen Coords
		function toScreen(x, y, z) {
			return [(((x+1.0)/2.0))*sx, (1-((y+1.0)/2.0))*sy, z];
			//return [(x + 1.0)/2.0 * sx, (y + 1.0)/2.0 * sy];
		}
		
		a = toScreen(a.x, a.y, a.z);
		b = toScreen(b.x, b.y, b.z);
		c = toScreen(c.x, c.y, c.z);
		
		drawTriangle(a, b, c, obj, i);
	}
}

export default {
	init, render, getBuffer, 
	rgba, rgb, drawPixel, setBackgroundColor,
	drawTriangle,
	Sprite, drawSprite,
	Object, drawObject,
	getScene
};
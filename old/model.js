import Vector from "./vector.js"

export function triangle(a, b, c) {
	return [
		[[a.x,a.y,a.z],[b.x,b.y,b.z],[c.x,c.y,c.z]]
	];
}

export function rectangle(p, a, b) {
	var A = triangle(p, p.add(a), p.add(b));
	var B = triangle(p.add(a).add(b), p.add(b), p.add(a));
	return A.concat(B);
}

export function box(a, b, c, d) {
	var A = rectangle(a, b, c);
	var B = rectangle(a, d, b);
	var C = rectangle(a, c, d);
	
	var p = a.add(b).add(c).add(d);
	var X = rectangle(p, b.negative(), c.negative());
	var Y = rectangle(p, d.negative(), b.negative());
	var Z = rectangle(p, c.negative(), d.negative());
	
	var F = A.concat(B).concat(C);
	var G = X.concat(Y).concat(Z);
	return F.concat(G);
}

export function displacePivot(model,x,y,z) {
	for (var j = 0; j < model.length; j++) {
		for (var i = 0; i < model[j].length; i++) {
			model[j][i][0] += x;
			model[j][i][1] += y;
			model[j][i][2] += z;
		}
	}
}

export function cube() {
	var model = box(new Vector(0,0,0),
					new Vector(1,0,0),
					new Vector(0,1,0),
					new Vector(0,0,1));
	displacePivot(model,-0.5,-0.5,-0.5);
	return model;
}


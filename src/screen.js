import { lerp_f, lerp_v3 } from './math'

export const ColorModel = {
    BW: { low: 0, up: 1, default: 0, bytes: 1 },
    RGB: { low: [0,0,0], up: [1,1,1], default: [0,0,0], bytes: 3 },
    RGBA: { low: [0,0,0,1], up: [1,1,1,1], default: [0,0,0,1], bytes: 4 }
}

export class Screen {
    constructor (width, height, clearValue) {
        this.width = width
        this.height = height
        this.clearValue = clearValue
    }
    makeBuffer(init = () => 0) {
        return new Array(this.width * this.height).map(init)
    }
    putPixel(x, y, value) {
        if (y < 0 || y >= this.height) return;
        if (x < 0 || x >= this.width) return;
        this.buffer[x*this.width + y] = value
    }
}

export class CanvasPixelScreen extends Screen {
    constructor(canvas, scale = 1, model = ColorModel.RGB) {
        super(canvas.width/scale, canvas.height/scale, model.default)
        this.model = model
        this.scale = scale
        this.canvas = canvas
        this.buffer = this.makeBuffer()
        this.device = undefined
    }

    makeBuffer(color = ColorModel.RGBA.default) {
        if (this.model === ColorModel.BW && Array.isArray(color)) {
            const m = (color[0]+color[1]+color[2]) / 3.0
            color = [m, m, m, 1];
        }
        else if (this.model === ColorModel.RGB && Array.isArray(color)) {
            color = [color[0], color[1], color[2], 1];
        }
        return new Uint8ClampedArray(4 * this.width * this.height).map((e,i) => color[i % 4]*255)
    }

    putPixel(x, y, v) {
        if (y < 0 || y >= this.height) return;
        if (x < 0 || x >= this.width) return;

        const i = ((this.height - Math.floor(y) - 1) * this.width + Math.floor(x)) * 4
        this.buffer[i + 0] = v*255
        this.buffer[i + 1] = v*255
        this.buffer[i + 2] = v*255
        this.buffer[i + 3] = 255
    }

    putPixelRGB(x, y, v) {
        if (y < 0 || y >= this.height) return;
        if (x < 0 || x >= this.width) return;
        if (this.model === ColorModel.BW) {
            const m = (v[0]+v[1]+v[2]) / 3.0
            v = [m, m, m, 1];
        }
        
        const i = ((this.height - Math.floor(y) - 1) * this.width + Math.floor(x)) * 4
        this.buffer[i + 0] = v[0]*255
        this.buffer[i + 1] = v[1]*255
        this.buffer[i + 2] = v[2]*255
        this.buffer[i + 3] = 255
    }

    putPixelRGBA(x, y, v) {
        if (y < 0 || y >= this.height) return;
        if (x < 0 || x >= this.width) return;
        if (this.model === ColorModel.BW) {
            m = v[0]+v[1]+v[2] / 3.0
            v = [m, m, m, 1];
        }

        const a = this.buffer[x*this.width + y]
        const b = v

        v = lerp_v3(a, b, b[3]); v[3] = 1
        if (this.model === ColorModel.RGBA) v[3] = a[3]+b[3]
        
        const i = ((this.height - Math.floor(y) - 1) * this.width + Math.floor(x)) * 4
        this.buffer[i + 0] = v[0]*255
        this.buffer[i + 1] = v[1]*255
        this.buffer[i + 2] = v[2]*255
        this.buffer[i + 3] = v[3]*255
    }

    refresh() {
        var ctx = this.canvas.getContext('2d')
        let cw = this.canvas.width
        let ch = this.canvas.height
        let w = this.width
        let h = this.height
        let s = this.scale
        let b = this.model.bytes

        if (!this.device) {
            ctx.clearRect(0, 0, cw, ch)
            this.device = ctx.getImageData(0, 0, cw, ch)
        }

        let dw = this.device.width
        let dh = this.device.height
        if (s != 1) {
            for (let y = 0; y < dh; y++) {
                for (let x = 0; x < dw; x++) {
                    let i = y * dw + x
                    let j = Math.floor(y/s) * w + Math.floor(x/s)
                    this.device.data[i*4+0] = this.buffer[j*4+0]
                    this.device.data[i*4+1] = this.buffer[j*4+1]
                    this.device.data[i*4+2] = this.buffer[j*4+2]
                    this.device.data[i*4+3] = this.buffer[j*4+3]
                }
            }
        } else {
            this.device.data.set(this.buffer)
        }
        
        ctx.putImageData(this.device, 0, 0);
        this.buffer = this.makeBuffer(this.clearValue)
    }
}


export function lerp(a, b, t) {
    if (!Object.isArray(a) && !Object.isArray(b)) return lerp_f(a, b, t)
    if (a.length == 3 && b.length == 3) return lerp_v3(a, b, t)
    if (a.length == 4 && b.length == 4) return lerp_v4(a, b, t)
    throw new Error('Invalid parameters')
}

export function lerp_f(a, b, t) {
    return a + t*(b-a)
}

export function lerp_v3(a, b, t) {
    return [
        lerp_f(a[0], b[0], t),
        lerp_f(a[1], b[1], t),
        lerp_f(a[2], b[2], t)
    ]
}

export function lerp_v4(a, b, t) {
    return [
        lerp_f(a[0], b[0], t),
        lerp_f(a[1], b[1], t),
        lerp_f(a[2], b[2], t),
        lerp_f(a[3], b[3], t)
    ]
}